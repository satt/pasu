-------------------------------------------------
  Pat_0 (-D-)   2   -   -   -   -   -   -   -   -   -   -   -   -   - 
  Pat_1 (-D-)   6   6   6   6   -   -   -   -   -   -   -   -   -   - 
  Pat_2 (-D-)   2   -   -   -   -   -   -   -   -   -   -   -   -   - 
  Pat_3 (-D-)   -   -   -   -   -   -   5   5   5   5   5   5   5   5 
  Pat_4 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   -   5 
  Pat_5 (-D-)   -   -   -   -   -   -   -   -   2   2   2   -   -   - 
  Pat_6 (-D-)   -   -   -   -   -   -   -   0   -   -   -   -   -   - 
  Pat_7 (-D-)   -   -   2   2   -   -   -   -   -   -   -   -   -   - 
  Pat_8 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   2   2  [+2]
  Pat_9 (-D-)   0   0   0   0   0   0   0   0   -   -   -   -   -   - 
 Pat_10 (-D-)   -   -   -   -   -   7   7   -   -   -   -   -   -   - 
 Pat_11 (-D-)   -   -   -   -   2   2   2   -   -   -   -   -   -   - 
 Pat_12 (-D-)   -   -   -   2   2   2   -   -   -   -   -   -   -   - 
 Pat_13 (-D-)   -   -   -   -   -   -   -   -   -   -   0   -   -   -  [+3]
 Pat_14 (-D-)   -   -   -   2   2   2   2   2   2   -   -   -   -   - 
 Pat_15 (-D-)   -   0   0   0   0   0   0   0   0   0   -   -   -   - 
 Pat_16 (-D-)   -   -   2   2   2   -   -   -   -   -   -   -   -   - 
 Pat_17 (-D-)   -   -   -   -   -   -   -   -   -   -   2   2   2   - 
 Pat_18 (-D-)   -   -   2   -   -   -   -   -   -   -   -   -   -   - 
 Pat_19 (-D-)   -   -   -   -   -   -   -   -   -   7   7   -   -   - 
 Pat_20 (-D-)   -   -   0   -   -   -   -   -   -   -   -   -   -   - 
 Pat_21 (-D-)   -   -   -   1   1   -   -   -   -   -   -   -   -   - 
 Pat_22 (-D-)   -   -   -   -   -   1   1   1   1   1   1   1   -   -  [+1]
 Pat_23 (-D-)   -   0   -   -   -   -   -   -   -   -   -   -   -   - 
 Pat_24 (-D-)   -   -   -   -   -   -   -   -   -   -   -   2   2   2  [+1]
 Pat_25 (-D-)   -   -   -   -   -   -   -   -   -   6   6   6   6   6 
 Pat_26 (-D-)   -   -   -   -   -   -   -   2   2   2   -   -   -   -  [+2]
 Pat_27 (-D-)   -   -   -   7   7   7   7   7   7   7   7   7   -   - 
 Pat_28 (-D-)   -   -   -   -   -   -   -   0   0   0   -   -   -   - 
 Pat_29 (-D-)   2   2   2   2   2   2   -   -   -   -   -   -   -   - 
 Pat_30 (-D-)   -   -   -   -   -   -   -   7   7   -   -   -   -   - 
 Pat_31 (-D-)   -   -   2   2   2   2   -   -   -   -   -   -   -   - 
 Pat_32 (-D-)   -   -   -   -   -   -   7   7   -   -   -   -   -   - 
 Pat_33 (-D-)   -   -   -   -   -   -   -   -   -   6   6   -   -   - 
 Pat_34 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   -   3 
 Pat_35 (-D-)   -   2   2   -   -   -   -   -   -   -   -   -   -   - 
 Pat_36 (-D-)   -   -   -   -   -   -   -   -   -   -   -   4   4   4 
 Pat_37 (-D-)   -   -   -   -   -   -   2   2   2   2   2   2   -   - 
 Pat_38 (-D-)   -   -   -   -   -   -   2   2   -   -   -   -   -   -  [+2]
 Pat_39 (-D-)   -   -   -   -   -   -   2   2   2   2   2   2   2   2 
 Pat_40 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   2   2 
 Pat_41 (-D-)   -   -   -   -   -   -   5   5   5   -   -   -   -   - 
 Pat_42 (-D-)   -   -   -   -   -   3   3   3   3   3   3   -   -   - 
 Pat_43 (-D-)   -   -   -   -   -   -   -   -   6   6   6   6   6   6 
 Pat_44 (-D-)   -   3   3   3   3   3   3   3   -   -   -   -   -   - 
 Pat_45 (-D-)   -   -   -   -   -   -   -   -   -   2   2   2   -   - 
 Pat_46 (-D-)   -   -   -   7   7   7   7   7   -   -   -   -   -   - 
 Pat_47 (-D-)   -   -   -   -   -   -   -   -   -   -   7   7   7   - 
 Pat_48 (-D-)   -   -   -   -   -   2   2   2   2   2   2   2   2   2 
 Pat_49 (-D-)   -   -   -   -   -   -   5   5   -   -   -   -   -   - 
-------------------------------------------------
Patient/room cost = 3950 (prop = 92 X 20, pref = 101 X 10, dept = 55 X 20, fixed gender = 0 X 50)
Gender cost = 0
Transfer cost = 0
Delay cost = 22
Overcrowd risk cost = 4
Seed: 1326122680
Time: 691.476
Total cost: 3976
