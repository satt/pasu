#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <sstream>

using namespace std;

enum Gender {MALE, FEMALE};
enum GenderPolicy {SAME_GENDER, MALE_ONLY, FEMALE_ONLY, TOGETHER};
enum Request {NEEDED, PREFERRED, DONT_CARE}; 
enum DoctoringLevel {COMPLETE, PARTIAL, NONE};

class Room 
{
public:
  Room(string n, unsigned c, unsigned d, string gender_policy);
  string name;
  unsigned capacity;
  unsigned department;
  GenderPolicy policy;
};

class Patient 
{
public:
  Patient(string n, unsigned ag, string g, unsigned re, unsigned en, 
	  unsigned le, unsigned var, unsigned max_ad);
  unsigned StayLength() const { return discharge_day - admission_day; }
  unsigned MaxDelay() const { return max_admission_day - admission_day; }
  bool Male() const { return gender == MALE; }
  bool Female() const { return gender == FEMALE; }

  bool OverstayRisk() const { return variability != 0; }

  string name;
  unsigned age;
  Gender gender;
  unsigned registration_day, admission_day, discharge_day;
  unsigned variability; // At present 0/1: with 0 = fixed discharge, 1 = risk of overstay by 1 one
  unsigned max_admission_day;
  unsigned preferred_room_capacity;
};

class PASU_Input
{
  friend ostream& operator<<(ostream& os, const PASU_Input& in);
public:
  PASU_Input(string file_name, const vector<unsigned>& weights);
  virtual ~PASU_Input() {}

  Patient* GetPatient(unsigned i) const { return patients[i]; }
  Room* GetRoom(unsigned i) const { return rooms[i]; }

  unsigned Rooms() const { return num_rooms; }
  unsigned RoomProperties() const { return num_room_properties; }
  unsigned Beds() const { return num_beds; }
  unsigned Departments() const { return num_departments; }
  unsigned Specialisms() const { return num_specialisms; }
  unsigned Patients() const { return num_patients; }
  unsigned Days() const { return num_days; }

  bool RoomProperty(unsigned i, unsigned j) const { return room_property[i][j]; }
  DoctoringLevel DeptSpecialismLevel(unsigned i, unsigned j) const { return  dept_specialism_level[i][j];  }
  unsigned PatientSpecialismNeeded(unsigned i) const { return  patient_specialism_needed[i];  }
  Request PatientPropertyRequest(unsigned i, unsigned j) const { return  patient_property_level[i][j]; }
  bool PatientNeededProperty(unsigned i, unsigned j) const { return  patient_property_level[i][j] == NEEDED; }
  bool PatientPreferredProperty(unsigned i, unsigned j) const { return  patient_property_level[i][j] == PREFERRED; }
  unsigned DepartmentMinAge(unsigned i) const { return department_age_limits[i].first; }
  unsigned DepartmentMaxAge(unsigned i) const { return department_age_limits[i].second; }
  unsigned PatientOverlap(unsigned i, unsigned j)  const { return patient_overlap[i][j]; }

  unsigned TotalPatientRoomCost(unsigned i, unsigned j) const { return total_patient_room_cost[i][j];  }

  bool PatientRoomAvailability(unsigned i, unsigned j) const { return patient_room_availability[i][j];  }
  
  GenderPolicy RoomGenderPolicy(unsigned r) const { return rooms[r]->policy; }

  unsigned LowerBound() const { return lower_bound; }
  // for updating the output
  int FindPatient(string name, unsigned i) const; // find the index of a patient, starting from i (for efficency)

  const unsigned PREFERRED_PROPERTY_WEIGHT, PREFERENCE_WEIGHT, SPECIALISM_WEIGHT, GENDER_WEIGHT, TRANSFER_WEIGHT, DELAY_WEIGHT, OVERCROWD_RISK_WEIGHT;
  const unsigned MAX_CAPACITY;
 
protected:
  string instance;
  unsigned num_rooms;
  unsigned num_room_properties;
  unsigned num_beds;
  unsigned num_departments; 
  unsigned num_specialisms; 
  unsigned num_patients; 
  unsigned num_days;

  unsigned lower_bound;

  vector<vector<bool> > room_property;
  vector<vector<DoctoringLevel> > dept_specialism_level;
  vector<vector<unsigned> > total_patient_room_cost; 
  vector<vector<bool> > patient_room_availability; 
  vector<unsigned> patient_specialism_needed; 

  vector<vector<Request> > patient_property_level;
  vector<vector<unsigned> > patient_overlap; //  number of days in which they are both in the hospital

  
  vector<pair<unsigned,unsigned> > department_age_limits;
  vector<unsigned> departments;
  vector<unsigned> specialisms;
  vector<unsigned> room_properties;
  vector<Room*> rooms;
  vector<Patient*> patients;
};

class PASU_Output
{
  friend ostream& operator<<(ostream& os, const PASU_Output& out);
public:
  PASU_Output(const PASU_Input& p_in, string file_name);
  int operator()(unsigned p, unsigned d) const { return day_assignment[p][d]; }
  int& operator()(unsigned p, unsigned d) { return day_assignment[p][d]; }
  bool Registered(unsigned p) const { return status[p] != -2; }
  bool Discharged(unsigned p) const { return discharged[p]; }
  bool Arrived(unsigned p) const { return status[p] >= 0; }
  unsigned AdmissionDay(unsigned p) const { assert(status[p] >= 0); return static_cast<unsigned>(status[p]); }
  unsigned PlannedAdmissionDay(unsigned p) const; 
  pair<unsigned,unsigned> ComputeCost();

private:
  const PASU_Input& in;
  vector<vector<int> > day_assignment; // -1 means no assignment
//   vector<unsigned> delay;
  vector<int> status; // -2: unknown, -1: registered, n: arrived at time n;
  vector<bool> discharged;
  unsigned patient_room_cost, gender_cost, transfer_cost, delay_cost, overcrowd_risk_cost, violations;
  unsigned properties_viol, preference_viol, department_viol, room_fixed_gender_viol;
};
  
// end header

Room::Room(string n, unsigned c, unsigned d, string gender_policy)
  : name(n), capacity(c), department(d)
{  
  if (gender_policy == "SG")
    policy = SAME_GENDER;
  else if (gender_policy == "Ma")
    policy = MALE_ONLY;
  else if (gender_policy == "Fe")
    policy = FEMALE_ONLY;
  else if (gender_policy == "All")
    policy = TOGETHER;
  else
    //error
    policy = SAME_GENDER;
}

Patient::Patient(string n, unsigned ag, string g, unsigned re, unsigned en, 
	  unsigned le, unsigned var, unsigned max_ad)
  : name(n)
{
  if (g == "Ma")
    gender = MALE;
  else if (g == "Fe")
    gender = FEMALE;
  else //error
    gender = FEMALE;
  age = ag;
  registration_day = re;
  admission_day = en;
  discharge_day = le;
  variability = var; 
  max_admission_day = max_ad;
}

PASU_Input::PASU_Input(string instance, const vector<unsigned>& weights)
  : PREFERRED_PROPERTY_WEIGHT(weights[0]), PREFERENCE_WEIGHT(weights[1]), SPECIALISM_WEIGHT(weights[2]), 
    GENDER_WEIGHT(weights[3]), TRANSFER_WEIGHT(weights[4]), DELAY_WEIGHT(weights[5]), OVERCROWD_RISK_WEIGHT(weights[6]), MAX_CAPACITY(6)
{
  string s, name, gen;
  const int BUF_SIZE = 256;
  char buf[BUF_SIZE], ch, lev;
  ifstream is(instance.c_str());

  if (is.fail())
    {
      cerr << "ERROR: Input file " << instance << " could not be open" << endl;
      exit(1);
    }

  unsigned p, p1, p2, r, pr, d, f, sp, dep, spec, cap; // id, val, daily_patients = 0, pi = 0,
  unsigned total_days = 0; // level, converted_level, 

  //	Read the header
  is.getline(buf,BUF_SIZE);
  is >> s >> num_departments;
  is >> s >> num_rooms;
  is >> s >> num_room_properties;
  is >> s >> num_patients;
  is >> s >> num_specialisms;
  is >> s >> num_days;

  num_beds = 0;

  room_property.resize(num_rooms,vector<bool>(num_room_properties,false));
  dept_specialism_level.resize(num_departments, vector<DoctoringLevel>(num_specialisms, NONE));
  department_age_limits.resize(num_departments, make_pair(0,120));
  patient_specialism_needed.resize(num_patients);
  patient_property_level.resize(num_patients, vector<Request>(num_room_properties,DONT_CARE));
  patient_overlap.resize(num_patients, vector<unsigned>(num_patients, 0));
  total_patient_room_cost.resize(num_patients, vector<unsigned>(num_rooms, 0));
  patient_room_availability.resize(num_patients, vector<bool>(num_rooms,true));


  // read the departments
  is >> ch;
  is.getline(buf,BUF_SIZE);

  for (d = 0; d < num_departments; d++) 
    {
      is >> name >> s;
      if (s == ">=")
	is >> department_age_limits[d].first;
      else if (s == "<=")
	is >> department_age_limits[d].second;
      // else: keep the default limits (0,120)
  
      is >> ch; // read (
      do{
	is >> spec >> ch;
	dept_specialism_level[d][spec] = COMPLETE;
      }
      while (ch == ',');


      is >> ch; // read ( or -
      if (ch == '(')
	do{
	  is >> spec >> ch;
	  dept_specialism_level[d][spec] = PARTIAL;
	}
	while (ch == ',');
    }

  is >> ch;
  is.getline(buf,BUF_SIZE);
  for (r = 0; r < num_rooms; r++) 
    {
      is >> name >> cap >> dep >> gen;
      Room *room = new Room(name, cap, dep, gen);
      num_beds+= cap; 

      is >> ch;
      if (ch == '(')
	{
	  do {
	    is >> f >> ch;
	    room_property[r][f] = true;
	  }
	  while (ch != ')');
	}
      rooms.push_back(room);
    }
 
  is >> ch;
  is.getline(buf,BUF_SIZE);

  for (p = 0; p < num_patients; p++) 
    { 
      unsigned age, entrance, leave, registration, variability, max_ad, 
	treatment;
      is >> name >> age >> gen >> ch  >> registration >> ch >> entrance  >> ch >> 
	leave  >> ch >> variability >> ch >> ch;
      if (ch == '*') // no limit to the max admission
	max_ad = num_days - (leave - entrance);
      else
	{
	  is >> ch >> max_ad;	  
	}
      is >> ch;


      total_days += leave - entrance;
      Patient* pat = new Patient(name,age,gen,registration,entrance,leave,variability,max_ad);
 
      is >> treatment;
      patient_specialism_needed[p] = treatment;

      is >> ch;
      if (ch == '*')
	pat->preferred_room_capacity = MAX_CAPACITY;
      else    
	is >> ch >> pat->preferred_room_capacity;

      is >> ch;
      if (ch == '(')
	{
	  do {
	    is >> f >> lev >> ch;
	    if (lev == 'n') // needed
	      patient_property_level[p][f] = NEEDED;
	    else
	      patient_property_level[p][f] = PREFERRED;	      
	  }
	  while (ch != ')');
	}
      patients.push_back(pat);
    }


  // Compute patient-patient overlap
  for (p1 = 0; p1 < num_patients-1; p1++) 
    for (p2 = p1+1; p2 < num_patients; p2++)  
      {
	for (d = patients[p1]->admission_day; d < patients[p1]->discharge_day; d++) 
	  if (patients[p2]->admission_day <= d && patients[p2]->discharge_day > d)
	    {
	      patient_overlap[p1][p2]++;
	      patient_overlap[p2][p1]++;
	    }
      }


  // Compute total patient room cost (and availability)
  for (p = 0; p < num_patients; p++)
    {
      sp = patient_specialism_needed[p];
      for (r = 0; r < num_rooms; r++)
	{
	  // Properties
	  for (pr = 0; pr < num_room_properties; pr++)
	    {
	      if (patient_property_level[p][pr] == NEEDED && !room_property[r][pr])
		patient_room_availability[p][r] = false;
	      if (patient_property_level[p][pr] == PREFERRED && !room_property[r][pr])
		total_patient_room_cost[p][r] += PREFERRED_PROPERTY_WEIGHT;
	    }
	  
	  // Preferences
	  if (patients[p]->preferred_room_capacity < rooms[r]->capacity)
	    total_patient_room_cost[p][r] += PREFERENCE_WEIGHT;
	  
	  // Specialism
	  dep = rooms[r]->department;
  	  if (dept_specialism_level[dep][sp] == PARTIAL)
  	    total_patient_room_cost[p][r] += SPECIALISM_WEIGHT; // * RoomDeptSpecialismLevel(r][sp] * (always 1)
	  if (dept_specialism_level[dep][sp] == NONE)
	    patient_room_availability[p][r] = false;

	  // Department age
	  if (department_age_limits[dep].first != 0 && patients[p]->age < department_age_limits[dep].first)
	    patient_room_availability[p][r] = false;
	  if (department_age_limits[dep].second != 0 && patients[p]->age > department_age_limits[dep].second)
	    patient_room_availability[p][r] = false;

	  // Gender 
	  if (rooms[r]->policy == MALE_ONLY && patients[p]->Female())
	    total_patient_room_cost[p][r] += GENDER_WEIGHT;
	  if (rooms[r]->policy == FEMALE_ONLY && patients[p]->Male())
	    total_patient_room_cost[p][r] += GENDER_WEIGHT;
	}
    }  

  // compute lower bound
  vector<int> patient_min_cost(num_patients, -1);
  for (p = 0; p < num_patients; p++)
    {
      for (r = 0; r < num_rooms; r++)
	if (patient_room_availability[p][r])
	  {
	    if (patient_min_cost[p] == -1 || total_patient_room_cost[p][r] < static_cast<unsigned>(patient_min_cost[p]))
	      patient_min_cost[p] = total_patient_room_cost[p][r];
	  }
      if (patient_min_cost[p] == -1)
	{
	  cerr << "Infeasible for patient " << GetPatient(p)->name << endl;
	}

      else
	lower_bound += static_cast<unsigned>(patient_min_cost[p]) * GetPatient(p)->StayLength();
    }
  is >> s;
}

int PASU_Input::FindPatient(string name, unsigned k) const
{
  unsigned i;
  for (i = k; i < num_patients; i++)
    if (patients[i]->name == name)
      return i;
  return -1;
}

PASU_Output::PASU_Output(const PASU_Input& p_in, string solution)
  : in(p_in), day_assignment(in.Patients(),vector<int>(in.Days(),-1)),
    status(in.Patients(),-2), discharged(in.Patients(),false)
{
  const int BUF_SIZE = 256;
  char buf[BUF_SIZE];
  ifstream is(solution.c_str());
  unsigned p, d;
  string room_string;

  if (is.fail())
    {
      cerr << "ERROR: Solution file " << solution << " could not be open" << endl;
      exit(1);
    }

  patient_room_cost = 0; 
  gender_cost = 0; 
  transfer_cost = 0; 
  delay_cost = 0; 
  overcrowd_risk_cost = 0;

  properties_viol = 0;
  preference_viol = 0;
  department_viol = 0;
  room_fixed_gender_viol = 0;
  violations = 0;

  is.ignore(BUF_SIZE, '\n');

  for (p = 0; p < in.Patients(); p++)
    {
      status[p] = -1;
      is >>  buf >> buf;
      for (d = 0; d < in.Days(); d++)
	{
	  is >> room_string;
	  if (room_string == "|") // ignore | (used for separation)
	    is >> room_string;
	  if (room_string == "-")
	    day_assignment[p][d] = -1;
	  else
	    {	      
	      day_assignment[p][d] = atoi(room_string.c_str());
	      if (status[p] == -1)
		status[p] = d;
	    }
	}
      is.ignore(BUF_SIZE, '\n'); // ignore the rest of the line
    }
}


ostream& operator<<(ostream& os, const PASU_Output& out)
{
  unsigned d, p, delay;
  os << "-------------------------------------------------" << endl;
  for (p = 0; p < out.in.Patients(); p++)
    {
      if (out.Registered(p))
	{
	  os << setw(7) << out.in.GetPatient(p)->name; 
	  if (out.Discharged(p))
	    os << " (-D-) ";
	  else if (out.Arrived(p))
	    os << " (-A-) ";
	  else
	    os << " (-R-) ";
	  for (d = 0; d < out.in.Days(); d++)
	    {
	      if (out(p,d) != -1)
		os << setw(3) << out(p,d) << " ";
	      else
		os << "  - ";
	    }
	  if (out.Registered(p) && 
	      out.PlannedAdmissionDay(p) > out.in.GetPatient(p)->admission_day)
	    {
	      delay = out.PlannedAdmissionDay(p) - out.in.GetPatient(p)->admission_day;
	      if (delay > 0)
		os << " [+" << delay << "]";
	    }
	  os << endl;
	}
    }
  os << "-------------------------------------------------" << endl;
  os << "Patient/room cost = " << out.patient_room_cost 
     << " (prop = " << out.properties_viol << " X " << out.in.PREFERRED_PROPERTY_WEIGHT 
     << ", pref = " << out.preference_viol << " X " << out.in.PREFERENCE_WEIGHT 
     << ", dept = " << out.department_viol <<  " X " << out.in.SPECIALISM_WEIGHT
     << ", fixed gender = " << out.room_fixed_gender_viol << " X " << out.in.GENDER_WEIGHT << ")" << endl;
  os << "Gender cost = " << out.gender_cost << endl;
  os << "Transfer cost = " << out.transfer_cost << endl;
  os << "Delay cost = " << out.delay_cost << endl;
  os << "Overcrowd risk cost = " << out.overcrowd_risk_cost << endl;

  return os;
}

unsigned PASU_Output::PlannedAdmissionDay(unsigned p) const 
{ 
  if (status[p] >= 0) 
    return AdmissionDay(p);
  else
    for (unsigned d = 0; d < in.Days(); d++)
      if (day_assignment[p][d] != -1)
	return d;
  assert(false);
  return 0;
}

pair<unsigned,unsigned> PASU_Output::ComputeCost()
{
  unsigned p, r, d, sp, rp, dep, potential_occupancy;
  int room, room1, room2;
  vector<vector<unsigned> > occupancy(in.Rooms(),vector<unsigned>(in.Days(),0));

  violations = 0;
  patient_room_cost = 0;
  gender_cost = 0;
  transfer_cost = 0;
  delay_cost = 0;
  overcrowd_risk_cost = 0;

  properties_viol = 0;
  preference_viol = 0;
  department_viol = 0;
  room_fixed_gender_viol = 0;

  // patient room cost
  for (p = 0; p < in.Patients(); p++)
    {
      sp = in.PatientSpecialismNeeded(p);
      for (d = 0; d < in.Days(); d++)
	{
	  room = day_assignment[p][d];
	  if (room != -1)
	    {
	      if (!in.PatientRoomAvailability(p,room))
		{
		  cerr << "AVAILABILITY VIOLATION: Patient " << in.GetPatient(p)->name << " in room " << in.GetRoom(room)->name << endl;
		  violations++;
		}
	      else
		patient_room_cost += in.TotalPatientRoomCost(p,room);

	      // detailed costs
	      for (rp = 0; rp < in.RoomProperties(); rp++)
		{
		  if (in.PatientPreferredProperty(p,rp) && !in.RoomProperty(room,rp))
		    {
		      properties_viol++;
		    }
		}
	      if (in.GetPatient(p)->preferred_room_capacity < in.GetRoom(room)->capacity)
		{
		  preference_viol++;
		}
	      dep = in.GetRoom(room)->department;
	      if (in.DeptSpecialismLevel(dep,sp) > 0)
		{
		  department_viol++;
		}
	      if (in.RoomGenderPolicy(room) == MALE_ONLY && in.GetPatient(p)->Female())
		{
		  room_fixed_gender_viol++;
		}
	      if (in.RoomGenderPolicy(room) == FEMALE_ONLY && in.GetPatient(p)->Male())
		{
		  room_fixed_gender_viol++;
		}
	    }
	}
    }

  // room capacity cost
  for (r = 0; r < in.Rooms(); r++)
    for (d = 0; d < in.Days(); d++)
      {
	for (p = 0; p < in.Patients(); p++)
	  if (day_assignment[p][d] != -1 && static_cast<unsigned>(day_assignment[p][d]) == r)
	    occupancy[r][d]++;
	if (occupancy[r][d] > in.GetRoom(r)->capacity)
	  {
	    violations += occupancy[r][d] - in.GetRoom(r)->capacity;
	    cerr << "CAPACITY VIOLATION: Room " << in.GetRoom(r)->name << " overcrowded (by " 
		 << occupancy[r][d] - in.GetRoom(r)->capacity << ") in day " << d << endl;
	  }	   
      }

  // gender cost
  for (d = 0; d < in.Days(); d++)
    {
      vector<unsigned> room_males(in.Rooms(),0), room_females(in.Rooms(),0);
      for (p = 0; p < in.Patients(); p++)
	{
	  room = day_assignment[p][d];
	  if (room != -1)
	    {
	      if (in.GetPatient(p)->Male())
		room_males[room]++;
	      else
		room_females[room]++;
	    }
	}	     
      for (r = 0; r < in.Rooms(); r++)
	if (in.RoomGenderPolicy(r) == SAME_GENDER)
	  {
	    if (min(room_males[r], room_females[r]) >= 1)
	      gender_cost += 1; //min(room_males[r], room_females[r]);
	  }
      // Removed because already included in the total patient/room cost
// 	else if (in.RoomGenderPolicy(r) == MALE_ONLY)
// 	  gender_cost += room_females[r];
// 	else if (in.RoomGenderPolicy(r) == FEMALE_ONLY)
// 	  gender_cost += room_males[r];
    }
  gender_cost *= in.GENDER_WEIGHT;


  // patient room cost
  for (p = 0; p < in.Patients(); p++)
    for (d = 1; d < in.Days(); d++)
      {
	room1 = day_assignment[p][d-1];
	room2 = day_assignment[p][d];
	if (room1 != -1 && room2 != -1 && room1 != room2)
	  transfer_cost++;
      }
  transfer_cost *= in.TRANSFER_WEIGHT;

  // delay cost
  for (p = 0; p < in.Patients(); p++)
    {      
      if (Registered(p) && 
	  PlannedAdmissionDay(p) > in.GetPatient(p)->admission_day)
	delay_cost += PlannedAdmissionDay(p) - in.GetPatient(p)->admission_day;
    }
  delay_cost *= in.DELAY_WEIGHT;  

  // overcrowd risk cost

  for (r = 0; r < in.Rooms(); r++)
    for (d = 1; d < in.Days(); d++)
      {
	potential_occupancy = occupancy[r][d];
	for (p = 0; p < in.Patients(); p++)
	  if (static_cast<unsigned>(day_assignment[p][d-1]) == r && day_assignment[p][d] == -1 && in.GetPatient(p)->OverstayRisk())
	    potential_occupancy++;
	if (potential_occupancy > in.GetRoom(r)->capacity)
	  {
	     overcrowd_risk_cost += potential_occupancy - in.GetRoom(r)->capacity;
	  }	   
      }
  overcrowd_risk_cost *= in.OVERCROWD_RISK_WEIGHT;

  return make_pair(violations, patient_room_cost + gender_cost + transfer_cost + delay_cost + overcrowd_risk_cost);
}

int main(int argc, char* argv[]) 
{
  if (argc != 3)
    {
      cerr << "Usage: " << argv[0] << " <input file> <solution file>" << endl;
      return 1;
    }

  vector<unsigned> weights(7); 
  weights[0] = 20;  // ROOM_PROPERTY
  weights[1] = 10;  // ROOM_PREFERENCE
  weights[2] = 20;  // SPECIALISM
  weights[3] = 50;  // GENDER POLICY
  weights[4] = 100; // TRANSFER
  weights[5] = 2;   // DELAY                    
  weights[6] = 1;   // OVERCROWD_RISK 

  PASU_Input in(argv[1], weights);
  PASU_Output out(in,argv[2]);      

  pair<unsigned,unsigned> cost = out.ComputeCost();

  cout << out << endl;
  cout << "Total cost = " << cost.second;
  if (cost.first > 0)
    cout << ", Violations = " << cost.first;
  cout << endl;

  return 0;
}
