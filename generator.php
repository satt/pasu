#!/usr/bin/php -q
<?php
if ($argc != 7)
  {
    echo "Usage: " . $argv[0] . " <departments> <rooms> <features> <patients> <specialisms> <days>\n";
    exit(1);
  }

date_default_timezone_set('Europe/Rome');

$departments = $argv[1];
$rooms = $argv[2];
$features = $argv[3];
$patients = $argv[4];
$specialisms =  $argv[5];
$days = $argv[6];

// probabilities (in %)
$pediatric_dept_prob = 10;
$geriatric_dept_prob = 10;
$prob_needed_feature = 10;
$over_stay_risk = 30; 

// other parameters
$max_number_of_main_specialisms_per_dept = (int)($specialisms/2) + 1;
$max_age = 90;
   // patients already present at time 0 (must have admission = 0, and short stay)
$incumbent_patients = (int) $patients/20; // 5%

// distributions
$length_distribution = array(10,20,20,10,8,8,8,8,6,6);
$max_length = count($length_distribution);
$available_capacities = array(1,2,4,6);
$policy_distributions = array(60,15,15,10);
$policies = array("SG","Ma","Fe","All");

// statistics
$total_capacity = 0;
$total_occupancy = 0;
$daily_occupancy = array_fill(0,$days, 0);

echo "Date: " . date('l jS \of F Y h:i:s A') . "\n";
echo "Departments: " . $departments . "\n";
echo "Rooms: " . $rooms . "\n";
echo "Features: " . $features . "\n";
echo "Patients: " . $patients . "\n";
echo "Specialisms: " . $specialisms . "\n";
echo "Days: " . $days . "\n";
echo "\nDEPARTMENTS (name, age_constraint, main_specialisms_list, aux_specialisms_list):\n";

for($i = 0; $i < $departments; $i++)
  {
    $s = "Dept_" . $i . " ";
    $type = rand(1,100);
    if ($type <= $pediatric_dept_prob)
      $s .= "<= 16";
    else if ($type <= $pediatric_dept_prob + $geriatric_dept_prob)
      $s .= ">= 65";
    else
      $s .= "*";
    $specs = array();
    $num_main_spec = rand(1,$max_number_of_main_specialisms_per_dept);
    $s .= " (";
    for($j = 1; $j <= $num_main_spec; $j++)
      {
	do 
	  $main_spec = rand(0,$specialisms-1);
	while (array_search($main_spec,$specs) !== FALSE);
	$specs[] = $main_spec;
      }
    sort($specs);
    for($j = 0; $j < $num_main_spec; $j++)
      if ($j < $num_main_spec-1)
	$s .= $specs[$j] . ",";
      else
	$s .= $specs[$j] . ") ";
    $min_aux_specs = min($specialisms,1);
    if ($specialisms - $num_main_spec <= $min_aux_specs + 1)
      $num_aux_spec = $min_aux_specs;
    else
      $num_aux_spec = rand($min_aux_specs, $specialisms - $num_main_spec - 1);

    if ($num_aux_spec == 0) // currently impossible: $min_aux_specs = 2
      $s .= "-";
    else
      {
	$aux_specs = array();
	$s .= "(";
	for($j = 1; $j <= $num_aux_spec; $j++)
	  {
	    do 
	      $aux_spec = rand(0,$specialisms-1);
	    while (array_search($aux_spec,$specs) !== FALSE || array_search($aux_spec,$aux_specs) !== FALSE);
	    $aux_specs[] = $aux_spec;
	  }
	sort($aux_specs);
  	for($j = 0; $j < $num_aux_spec; $j++)	 
	  if ($j < $num_aux_spec - 1)
	      $s .= $aux_specs[$j] . ",";
	    else
	      $s .= $aux_specs[$j] . ")";
      }
    echo $s . "\n";
  }

exit(1);

echo "\nROOMS (name, capacity, dept_index, gender_policy (SG/Ma/Fe/All), features_list):\n";

for($i = 0; $i < $rooms; $i++)
  {
    $s = $i;
    $capacity = $available_capacities[rand(0,count($available_capacities)-1)];
    $total_capacity += $capacity;
    $s .= " " . $capacity;
    $department = rand(0,$departments-1);    
    $s .= " " . $department;
    $val = rand(1,100);
    $policy = SelectFromDistribution($policy_distributions);
    $s .= " " . $policies[$policy] .  " ";

    $num_features = rand(2, $features);
    if ($num_features == 0)
      $s .= "-";
    else
      {
	$selected_features = array();
	$s .= "(";
	for($j = 1; $j <= $num_features; $j++)
	  {
	    do 
	      $feature = rand(0,$features-1);
	    while (array_search($feature,$selected_features) !== FALSE);
	    $selected_features[] = $feature;
	  }
	sort($selected_features);
	for($j = 0; $j < $num_features; $j++)
	  {
	    if ($j < $num_features - 1)
	      $s .= $selected_features[$j] . ",";
	    else
	      $s .= $selected_features[$j] . ")";
	  }
      }
    echo $s . "\n";
  }

echo "\nPATIENTS (name, age, gender, [registration, admission, discharge, variability, max_admission], treatment, preferred_capacity, room_property_list):\n";

for($i = 0; $i < $patients; $i++)
  {
    $s = "Pat_" . $i . " ";
    $age = rand(1,$max_age);
    $s .= $age . " ";
    if (rand(0,1))
      $s .= "Fe ";
    else
      $s .= "Ma ";

    $length = SelectFromDistribution($length_distribution) + 1;
    if ($i < $incumbent_patients)
      {
	$admission = 0;
	$length = (int) (($length + 1)/2);
      }
    else
      $admission = rand(0,$days-1);

    $length = min($length, $days - $admission);
    $discharge = $admission + $length;
    $registration = rand(0, $admission);
    $variability = (rand(1,100) <=  $over_stay_risk) ? 1 : 0; 

    if (rand(0,1) == 0) // 50%
      $max_admission = "<=" . rand($admission, $days - $length);
    else
      $max_admission = "*";
      
    for ($j = $admission; $j < $admission + $length; $j++)
      $daily_occupancy[$j]++;
    $total_occupancy += $length;

    $s .= "[" . $registration . ", " . $admission . ", " . $discharge  . ", " 
      .  $variability . ", " . $max_admission . "] ";

    $treatment = rand(0,$specialisms-1);
    $s .= $treatment;

    $capacity_preference = $available_capacities[rand(0,count($available_capacities)-1)];
    if ($capacity_preference == $available_capacities[count($available_capacities)-1])
      $s .= " * ";
    else
      $s .= " <=" . $capacity_preference . " ";
    
    // biased selection (quadratic)
    $squared_num_features = rand(0, $features*$features);
    $num_features = ceil(sqrt($squared_num_features));
    if ($num_features == 0)
      $s .= "-";
    else
      {
	$selected_features = array();
	$s .= "(";
	for ($j = 1; $j <= $num_features; $j++)
	  {
	    do 
	      $feature = rand(0,$features-1);
	    while (array_search($feature,$selected_features) !== FALSE);
	    $selected_features[] = $feature;
	  }
	sort($selected_features);
	for ($j = 0; $j < $num_features; $j++)
	  {
	    $need = (rand(1,100) <= $prob_needed_feature) ? "n" : "p";
	    if ($j < $num_features - 1)
	      $s .= $selected_features[$j] . $need . ",";
	    else
	      $s .= $selected_features[$j] . $need . ")";
	  }
      }

    echo $s . "\n";
  }
echo "\nEND.\n";

echo "--------------------------------------------------\n";
echo "Total capacity = " . $total_capacity . " X " . $days . " = " . $total_capacity*$days . " beds*days\n";
echo "Total occupancy = " . $total_occupancy . " (" . $total_occupancy/($total_capacity*$days) * 100 . "%)\n";
echo "Daily occupancy = ";
foreach($daily_occupancy as $day_occupancy)
  echo (int) (100 * $day_occupancy / $total_capacity) . "%, ";
echo "\n";

function SelectFromDistribution($distribution)
{
  $n = count($distribution);
  $sum = 0;
  for ($i = 0; $i < $n; $i++)
    $sum += $distribution[$i];

  $val = rand(1,$sum);

  $accumulated_prob = 0;
  for ($j = 0; $j < $n; $j++)
    {
      if ($val <= $distribution[$j] + $accumulated_prob)
	break;
      $accumulated_prob +=  $distribution[$j];
    }
  return $j;
}
?>
    
