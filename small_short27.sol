-------------------------------------------------
  Pat_0 (-D-)   5   5   5   5   5   -   -   -   -   -   -   -   -   - 
  Pat_1 (-D-)   6   -   -   -   -   -   -   -   -   -   -   -   -   - 
  Pat_2 (-D-)   3   3   -   -   -   -   -   -   -   -   -   -   -   - 
  Pat_3 (-D-)   -   -   -   7   7   7   7   -   -   -   -   -   -   - 
  Pat_4 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   6   6 
  Pat_5 (-D-)   -   -   -   -   -   -   -   -   -   2   2   2   -   - 
  Pat_6 (-D-)   -   -   -   -   -   -   -   -   -   -   5   5   5   5 
  Pat_7 (-D-)   -   -   -   -   -   -   -   3   3   3   3   3   3   -  [+1]
  Pat_8 (-D-)   -   -   -   2   2   2   2   2   2   -   -   -   -   - 
  Pat_9 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   7   7 
 Pat_10 (-D-)   -   -   -   -   -   -   -   -   5   5   5   5   5   5 
 Pat_11 (-D-)   -   -   -   -   -   -   -   -   -   -   -   6   6   6 
 Pat_12 (-D-)   -   -   -   -   -   -   7   7   7   7   7   7   7   7  [+1]
 Pat_13 (-D-)   -   -   -   5   5   5   5   5   5   5   5   5   -   - 
 Pat_14 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   -   4 
 Pat_15 (-D-)   -   -   -   -   -   5   5   5   -   -   -   -   -   - 
 Pat_16 (-D-)   -   -   -   -   -   -   -   -   5   5   -   -   -   - 
 Pat_17 (-D-)   -   -   7   7   7   7   -   -   -   -   -   -   -   - 
 Pat_18 (-D-)   7   7   -   -   -   -   -   -   -   -   -   -   -   - 
 Pat_19 (-D-)   7   7   -   -   -   -   -   -   -   -   -   -   -   - 
 Pat_20 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   5   5 
 Pat_21 (-D-)   -   -   7   7   7   7   7   7   7   7   7   -   -   - 
 Pat_22 (-D-)   -   -   -   -   -   -   7   7   7   7   7   7   -   -  [+1]
 Pat_23 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   -   3 
 Pat_24 (-D-)   -   -   -   -   -   -   -   -   -   -   1   1   1   1 
 Pat_25 (-D-)   -   -   -   -   5   5   -   -   -   -   -   -   -   - 
 Pat_26 (-D-)   3   3   -   -   -   -   -   -   -   -   -   -   -   - 
 Pat_27 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   5   5  [+3]
 Pat_28 (-D-)   -   -   -   -   -   -   3   3   3   3   3   3   3   3  [+3]
 Pat_29 (-D-)   -   -   -   -   -   -   -   -   -   -   0   0   -   -  [+1]
 Pat_30 (-D-)   -   -   -   -   -   -   -   -   -   -   -   7   7   7 
 Pat_31 (-D-)   -   -   0   0   -   -   -   -   -   -   -   -   -   - 
 Pat_32 (-D-)   -   -   -   -   -   -   -   1   1   1   1   -   -   - 
 Pat_33 (-D-)   -   -   -   3   3   -   -   -   -   -   -   -   -   - 
 Pat_34 (-D-)   -   -   -   -   -   4   4   4   4   4   -   -   -   -  [+1]
 Pat_35 (-D-)   -   -   7   7   7   7   -   -   -   -   -   -   -   - 
 Pat_36 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   -   6 
 Pat_37 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   0   0  [+1]
 Pat_38 (-D-)   -   -   -   -   -   -   -   7   7   7   7   7   7   7  [+1]
 Pat_39 (-D-)   -   -   -   -   -   3   3   -   -   -   -   -   -   - 
 Pat_40 (-D-)   -   -   -   -   -   -   -   -   -   -   1   1   1   - 
 Pat_41 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   -   1 
 Pat_42 (-D-)   -   -   -   -   -   -   6   6   6   6   6   6   -   -  [+1]
 Pat_43 (-D-)   -   -   -   -   -   -   1   1   -   -   -   -   -   - 
 Pat_44 (-D-)   -   -   -   -   -   -   5   5   5   5   5   5   -   -  [+3]
 Pat_45 (-D-)   -   5   5   5   5   5   5   5   -   -   -   -   -   - 
 Pat_46 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   4   4 
 Pat_47 (-D-)   -   -   6   6   6   6   -   -   -   -   -   -   -   - 
 Pat_48 (-D-)   -   -   3   3   3   -   -   -   -   -   -   -   -   -  [+1]
 Pat_49 (-D-)   -   -   -   -   -   -   2   2   2   2   2   2   2   - 
-------------------------------------------------
Patient/room cost = 1590 (prop = 17 X 20, pref = 69 X 10, dept = 28 X 20, fixed gender = 0 X 50)
Gender cost = 50
Transfer cost = 0
Delay cost = 36
Overcrowd risk cost = 5
Seed: 1326136330
Time: 706.507
Total cost: 1681
