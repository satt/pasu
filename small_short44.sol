-------------------------------------------------
  Pat_0 (-D-)   1   -   -   -   -   -   -   -   -   -   -   -   -   - 
  Pat_1 (-D-)   1   -   -   -   -   -   -   -   -   -   -   -   -   - 
  Pat_2 (-D-)   6   6   6   -   -   -   -   -   -   -   -   -   -   - 
  Pat_3 (-D-)   -   -   1   1   1   1   1   1   -   -   -   -   -   -  [+1]
  Pat_4 (-D-)   -   -   -   -   -   -   -   -   -   -   -   5   5   5 
  Pat_5 (-D-)   -   -   -   -   -   -   -   -   1   -   -   -   -   -  [+2]
  Pat_6 (-D-)   -   -   -   -   -   -   -   -   -   5   5   5   5   5 
  Pat_7 (-D-)   -   -   -   -   6   6   -   -   -   -   -   -   -   - 
  Pat_8 (-D-)   -   -   -   -   -   -   -   -   6   -   -   -   -   - 
  Pat_9 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   -   0 
 Pat_10 (-D-)   -   -   -   -   -   -   -   -   -   5   5   5   -   - 
 Pat_11 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   0   0 
 Pat_12 (-D-)   -   -   -   -   -   -   6   6   -   -   -   -   -   - 
 Pat_13 (-D-)   -   -   -   -   -   -   -   -   -   1   1   1   1   1 
 Pat_14 (-D-)   1   1   -   -   -   -   -   -   -   -   -   -   -   - 
 Pat_15 (-D-)   -   -   -   -   -   -   -   -   3   3   3   3   3   3 
 Pat_16 (-D-)   -   -   -   -   -   -   -   -   1   1   -   -   -   - 
 Pat_17 (-D-)   -   -   -   -   -   -   -   5   5   5   5   -   -   - 
 Pat_18 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   6   6 
 Pat_19 (-D-)   -   -   -   -   -   -   -   -   6   6   6   6   -   - 
 Pat_20 (-D-)   -   -   1   1   1   1   1   1   -   -   -   -   -   -  [+2]
 Pat_21 (-D-)   -   -   -   -   -   -   7   7   -   -   -   -   -   -  [+5]
 Pat_22 (-D-)   -   -   -   3   3   3   -   -   -   -   -   -   -   - 
 Pat_23 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   4   4 
 Pat_24 (-D-)   -   -   -   -   -   -   6   6   6   -   -   -   -   - 
 Pat_25 (-D-)   -   -   -   -   1   -   -   -   -   -   -   -   -   - 
 Pat_26 (-D-)   -   -   -   -   4   4   4   4   4   4   4   4   -   - 
 Pat_27 (-D-)   -   -   -   -   -   -   -   5   -   -   -   -   -   - 
 Pat_28 (-D-)   -   -   -   -   -   -   -   -   5   5   5   5   5   5 
 Pat_29 (-D-)   -   -   -   -   -   5   5   -   -   -   -   -   -   -  [+2]
 Pat_30 (-D-)   -   -   -   -   -   5   5   5   -   -   -   -   -   -  [+4]
 Pat_31 (-D-)   -   -   -   -   -   -   -   -   -   -   1   1   -   -  [+3]
 Pat_32 (-D-)   -   -   -   -   -   -   6   6   6   -   -   -   -   -  [+1]
 Pat_33 (-D-)   -   -   -   6   6   -   -   -   -   -   -   -   -   - 
 Pat_34 (-D-)   -   -   -   -   1   1   -   -   -   -   -   -   -   - 
 Pat_35 (-D-)   -   -   -   -   -   -   -   -   -   -   -   3   3   - 
 Pat_36 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   6   6 
 Pat_37 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   6   6 
 Pat_38 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   -   1 
 Pat_39 (-D-)   -   -   -   -   -   6   -   -   -   -   -   -   -   - 
 Pat_40 (-D-)   -   -   -   5   5   -   -   -   -   -   -   -   -   - 
 Pat_41 (-D-)   -   -   -   -   -   -   -   -   -   -   -   1   1   1  [+2]
 Pat_42 (-D-)   -   -   -   -   -   -   1   1   1   1   1   1   1   1  [+1]
 Pat_43 (-D-)   -   -   -   -   -   -   -   -   -   -   -   6   6   6 
 Pat_44 (-D-)   -   -   -   -   -   1   1   1   1   1   1   -   -   - 
 Pat_45 (-D-)   -   -   -   -   -   -   -   -   -   -   -   -   6   6 
 Pat_46 (-D-)   -   -   -   -   -   -   -   -   -   -   -   3   3   3 
 Pat_47 (-D-)   -   4   4   -   -   -   -   -   -   -   -   -   -   - 
 Pat_48 (-D-)   -   -   -   -   -   -   -   -   -   -   -   3   3   3 
 Pat_49 (-D-)   -   -   -   -   -   -   -   -   -   -   6   6   6   6 
-------------------------------------------------
Patient/room cost = 2570 (prop = 41 X 20, pref = 65 X 10, dept = 55 X 20, fixed gender = 0 X 50)
Gender cost = 0
Transfer cost = 0
Delay cost = 46
Overcrowd risk cost = 2
Seed: 1326147617
Time: 713.86
Total cost: 2618
